function updateContent(){
    const xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onload = onLoadUpdateContent;
    xmlHttpRequest.open("GET","http://localhost:8080/ajaxgb/guestbook",true);
    xmlHttpRequest.send(null);
}

function onLoadUpdateContent(){
    const content = document.getElementById("content");
    let responseText = this.responseText;
    const items = JSON.parse(responseText);
    while(content.firstChild){
        content.removeChild(content.firstChild);
    }
    for(let item of items){
        const d = document.createElement('div');
        const t = document.createTextNode(`${item.date} : ${item.name} ${item.message}`);
        d.appendChild(t);
        content.appendChild(d);
    }
}

function addItem(e){
    e.preventDefault();
    const nameField = document.getElementById("name");
    const messageField = document.getElementById("message");

    const item =
    {
            name:nameField.value,
            message: messageField.value
    }

    const xmlHttpRequest = new XMLHttpRequest();
    xmlHttpRequest.onload = onLoadAddItem;
    xmlHttpRequest.open("POST","http://localhost:8080/ajaxgb/guestbook",true);
    xmlHttpRequest.send(JSON.stringify(item));
}

function onLoadAddItem(){
    const form = document.getElementById("form");
    form.reset();
    updateContent();
}

function init(){
    updateContent();
    const form = document.getElementById("form");
    form.addEventListener("submit",addItem);
}

window.addEventListener("load",init);