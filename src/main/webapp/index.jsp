<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Guestbook</title>
    <script src="guestbook.js"></script>
</head>
<body>
    <div>
        <h1>Guestbook</h1>
        <div id="content"></div>
        <form id="form">
            Name<br/>
            <input type="text" id="name" name="name" required/><br/>
            Message<br/>
            <textarea cols="50" row="4" id="message" name="message" required></textarea><br/>
            <input type="submit" value="Submit"/>
        </form>
    </div>
</body>
</html>