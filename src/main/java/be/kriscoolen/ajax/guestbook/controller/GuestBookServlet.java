package be.kriscoolen.ajax.guestbook.controller;

import be.kriscoolen.ajax.guestbook.domain.GuestBookBean;
import be.kriscoolen.ajax.guestbook.repository.GuestBookDao;

import javax.annotation.Resource;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

@WebServlet(value="/guestbook")
public class GuestBookServlet extends HttpServlet {
    @Resource(name="GuestBookDS")
    private DataSource dataSource;
    private GuestBookDao dao;
    private Jsonb jsonb;

    @Override
    public void init()  {
        dao = new GuestBookDao();
        dao.setDataSource(dataSource);
        jsonb = JsonbBuilder.create();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try(PrintWriter out= resp.getWriter()){
            resp.setContentType("text/plain");
            resp.addHeader("Access-Control-Allow-Origin","*");
            List<GuestBookBean> items = dao.getGuestBookItems();
            jsonb.toJson(items,out);
        } catch (SQLException e){
            throw new ServletException("Exception",e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try{
            Reader reader = req.getReader();
            GuestBookBean item = jsonb.fromJson(reader,GuestBookBean.class);
            item.setDate(LocalDateTime.now());
            dao.addGuestBookItem(item);
        } catch(Exception ex){
            throw new ServletException(ex);
        }
    }
}
