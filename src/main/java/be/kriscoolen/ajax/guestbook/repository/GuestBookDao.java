package be.kriscoolen.ajax.guestbook.repository;

import be.kriscoolen.ajax.guestbook.domain.GuestBookBean;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GuestBookDao {

    private static final String GET_QUERY =
            "select Date, Name, Message from GuestBook order by Date";

    private static final String UPDATE_QUERY =
            "insert into GuestBook (Date, Name, Message) values (?,?,?)";

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<GuestBookBean> getGuestBookItems() throws SQLException {
        try(Connection con = getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(GET_QUERY))
        {
            List<GuestBookBean> messages = new ArrayList<>();
            while(rs.next()){
                GuestBookBean bean =
                        new GuestBookBean
                        (
                        rs.getTimestamp(1).toLocalDateTime(),
                        rs.getString(2),
                        rs.getString(3)
                        );
                messages.add(bean);
            }
            return messages;
        }
    }

    public void addGuestBookItem(GuestBookBean item) throws SQLException{
        try(Connection con = getConnection();
            PreparedStatement preparedStatement = con.prepareStatement(UPDATE_QUERY))
        {
            preparedStatement.setTimestamp(1,Timestamp.valueOf(item.getDate()));
            preparedStatement.setString(2,item.getName());
            preparedStatement.setString(3,item.getMessage());
            preparedStatement.executeUpdate();
        }
    }

    private Connection getConnection() throws SQLException{
        return dataSource.getConnection();
    }
}
